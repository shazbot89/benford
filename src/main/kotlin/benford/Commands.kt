package benford

import benford.io.FileParser
import com.github.ajalt.clikt.core.CliktCommand
import com.github.ajalt.clikt.parameters.arguments.argument
import com.github.ajalt.clikt.parameters.types.file
import java.io.File

class Start : CliktCommand(name = "benford") {

    private val file: File by argument(help = "File containing dataset to analyze")
            .file(
                    mustExist = true,
                    mustBeReadable = true,
                    canBeDir = false
            )

    override fun run() {
        val data = FileParser(file).data
        val analyzer = Analyzer(data)
        echo("Analysis of ${file.name} complete...\n")
        echo("Total:\t${analyzer.data.count()}")
        analyzer.distributionCount.forEach { println("\t$it") }
        echo("Percentage:")
        analyzer.distributionPercent.forEach { println("\t$it") }
    }

}