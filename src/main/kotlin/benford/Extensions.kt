package benford


/**
 * Return the first digit of an [Int].
 *
 * @receiver the [Int] to take the first digit of
 * @return first digit of the [Int]
 */
val Int.firstDigit: Int
    get() = Integer.parseInt("$this".first().toString())

/**
 * Returns a [Map] with the leading digits as the [Map.keys] and count as the [Map.values].
 *
 * @receiver The [List] of [Int]s to map
 */
val List<Int>.toBenfordMap: Map<Int, Int>
    get() = this.groupingBy { it.firstDigit }.eachCount().toSortedMap()