package benford

class Analyzer(val data: List<Int>) {

    /**
     * Returns a [Map] of each beginning digit and it's respective count.
     */
    val distributionCount: Map<Int, Int> by lazy { data.toBenfordMap }

    /**
     * Returns a [Map] of each beginning digit and it's respective count.
     */
    val distributionPercent: Map<Int, Double> by lazy { 
        distributionCount.mapValues { it.value / data.count().toDouble() }
     }

}