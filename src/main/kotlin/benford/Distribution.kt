package benford

/**
 * Enumeration of [Benford's Law][https://en.wikipedia.org/wiki/Benford%27s_law#Definition] digit distribution.
 */
enum class Distribution(val digit: Int, val percent: Double) {
    ONE(1, 30.1),
    TWO(2, 17.6),
    THREE(3, 12.5),
    FOUR(4, 9.7),
    FIVE(5, 7.9),
    SIX(6, 6.7),
    SEVEN(7, 5.8),
    EIGHT(8, 5.1),
    NINE(9, 4.6)
}