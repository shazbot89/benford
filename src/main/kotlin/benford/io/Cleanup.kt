package benford.io

typealias CleanupRules = Set<Rule>

interface Rule {
    fun apply(items: List<String?>): List<String?>
}

class RemoveNulls : Rule {
    override fun apply(items: List<String?>): List<String> = items.filterNotNull()
}

class DecimalToWhole : Rule {
    override fun apply(items: List<String?>): List<String?> = items.map { item ->
        try {
            item?.toDouble()?.toInt().toString()
        } catch (nfe: NumberFormatException) {
            item
        }
    }
}

fun allRules(): Array<out Rule> = arrayOf(
        RemoveNulls(),
        DecimalToWhole()
)

fun List<String>.cleanup(vararg rules: Rule = allRules()): List<String> {
    var cleanedList: List<String> = this
    rules.forEach { rule -> cleanedList = rule.apply(cleanedList) as List<String> }
    return cleanedList
}