package benford.io

import java.io.File

class FileParser(private val file: File) {

    val data: List<Int>
        get() = when (file.extension) {
            FileTypes.CSV.extension -> parseCsv(file)
            else -> parseCsv(file)
        }

    private fun parseCsv(file: File): List<Int> = file.bufferedReader()
            .readLines()
            .cleanup()
            .map { Integer.parseInt(it) }

    enum class FileTypes(val extension: String) {
        CSV("csv")
    }

}
