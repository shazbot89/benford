package benford.io

import org.junit.Test
import kotlin.test.assertEquals

internal class CleanupKtTest {

    @Test
    fun `RemoveNulls removes nulls`() {
        val sample: List<String?> = listOf(null, 1, 2, 3, 4, null, 5, 6, 7, null).map { it?.toString() }
        val expected: List<String> = listOf(1, 2, 3, 4, 5, 6, 7).map { it.toString() }
        val actual: List<String> = RemoveNulls().apply(sample)

        assertEquals(expected, actual)
    }

    @Test
    fun `DecimalToWhole converts Doubles to Ints`() {
        val sample: List<String> = listOf(1, 2.1, 3.10409, 5.10294901, 5.999999999).map { it.toString() }
        val expected: List<String> = listOf(1, 2, 3, 5, 5).map { it.toString() }
        val actual: List<String?> = DecimalToWhole().apply(sample)

        assertEquals(expected, actual)
    }

}