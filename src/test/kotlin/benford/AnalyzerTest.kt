package benford

import kotlin.test.Test
import kotlin.test.assertEquals

internal class AnalyzerTest {

    @Test
    fun `Correctly analyzes benfordy data set`() {
        val expected = SampleData.benfordySampleData.toBenfordMap
        val actual = Analyzer(SampleData.benfordySampleData).distributionCount

        assertEquals(expected, actual)
    }

    @Test
    fun `Correctly analyzes random data set`() {
        val expected = SampleData.randomSampleData.toBenfordMap
        val actual = Analyzer(SampleData.randomSampleData).distributionCount

        assertEquals(expected, actual)
    }

}