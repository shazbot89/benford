package benford

import com.github.ajalt.clikt.core.BadParameterValue
import com.github.ajalt.clikt.core.MissingArgument
import java.io.File
import kotlin.test.Test
import kotlin.test.assertFailsWith

internal class StartTest {

    @Test
    fun `Fails without argument`() {
        val noFileArg = emptyList<String>()

        assertFailsWith<MissingArgument> { Start().parse(noFileArg) }
    }

    @Test
    fun `Fails with directory argument`() {
        val directoryPath = File("sample/").absolutePath
        val directoryArg = arrayListOf(directoryPath)

        assertFailsWith<BadParameterValue> { Start().parse(directoryArg) }
    }

}