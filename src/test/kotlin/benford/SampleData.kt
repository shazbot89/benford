package benford

import kotlin.math.roundToInt
import kotlin.random.Random
import kotlin.random.nextInt

/**
 * Sample data for testing.
 */
object SampleData {

    private const val sampleSize = 60

    /**
     * "Benfordy" Sample data as a [List] of [Int]s. Loops through [Distribution] and uses each [Distribution.digit]
     * and [Distribution.percent] for the [sampleSize] to populate.
     */
    val benfordySampleData: List<Int> = Distribution.values()
            .flatMap { dist -> Array(dist.percent.of(sampleSize)) { dist.digit.plusRandom() }.toList() }

    /**
     * Random sample data as a [List] of [Int]s. Uses [Random] [Int]s in the range of 0 to 9999 to populate.
     */
    val randomSampleData: List<Int> = List(sampleSize) { Random.nextInt(0..9999) }

    /**
     * Attaches a random integer in the given [range] to the receiving [Int].
     * ```
     * 1.plusRandom(0..99) // 146
     * ```
     *
     * @receiver the leading digit as an [Int]
     * @param range the range for the random number, default: 0..999
     */
    private fun Int.plusRandom(range: IntRange = 0..999): Int = Integer.parseInt("$this${(range).random()}")

    /**
     * Provides a rounded percentage of the [sample].
     *
     * @receiver the percentage as a [Double]
     * @param sample the sample size as an [Int]
     */
    private fun Double.of(sample: Int): Int = (this * sample / 100).roundToInt()

}